package com.example.msyo19.primer_avance;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bo = (Button) findViewById(R.id.btn);
        bo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputLayout nombre = findViewById(R.id.Nom);
                TextInputLayout apellidop = findViewById(R.id.Appa);
                TextInputLayout apellidom = findViewById(R.id.Apma);
                TextView fecha = findViewById(R.id.fecha);
                Spinner estado = findViewById(R.id.Estado);
                RadioButton hombre = findViewById(R.id.hombre);
                RadioButton mujer = findViewById(R.id.mujer);
                String sexo;

                if(hombre.isChecked()==true){sexo = getString(R.string.mt);}else{sexo = getString(R.string.fdt);}


                Persona persona = new Persona(nombre.getEditText().getText().toString(),apellidop.getEditText().getText().toString(),apellidom.getEditText().getText().toString(),sexo,fecha.getText().toString(),estado.getSelectedItem().toString());

                Intent intent= new Intent(v.getContext(),Main2Activity.class);

                intent.putExtra("persona",persona);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.cerrar){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
