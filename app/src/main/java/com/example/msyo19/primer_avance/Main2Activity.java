package com.example.msyo19.primer_avance;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = this.getIntent();
        Persona persona = intent.getParcelableExtra("persona");
        TextView texto = findViewById(R.id.Resultado);
        texto.setText(getString(R.string.nombret)+": "+persona.getNombre()+"\n"
                +getString(R.string.apept)+": "+persona.getApp()+"\n"
                +getString(R.string.apemt)+": "+persona.getApm()+"\n"
                +getString(R.string.sexot)+": "+persona.getSexo()+"\n"
                +getString(R.string.fechadt)+": "+persona.getFecha()+"\n"
                +getString(R.string.estadt)+": "+persona.getEstado()+"\n");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.cerrar){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
